const inputEl = document.getElementById('engine');
const submitBtn = document.getElementById('submitBtn');


// submitBtn.addEventListener('click', getMeal);

// function getMeal(){
//     const input = inputEl.value.trim();
//         console.log(input);
//         console.log(`https://www.themealdb.com/api/json/v1/1/search.php?s=${input}`)

//         fetch(`https://www.themealdb.com/api/json/v1/1/search.php?s=${input}`)
//         .then(res => res.json())
//         .then(data => console.log(data));
// }

// When input field is and 'Enter' pressed, it will fetch API
inputEl.addEventListener("keydown", function (event) {
    if (event.key === "Enter") {
        const input = inputEl.value.trim();
        console.log(input);
        console.log(`https://www.themealdb.com/api/json/v1/1/search.php?s=${input}`)

        fetch(`https://www.themealdb.com/api/json/v1/1/search.php?s=${input}`)
            .then(res => res.json())
            .then(data => {
                console.log(data.meals)

                let arr = [];
                data.meals.forEach(meal => {
                    let element = `<div class="col">
                <div class="card text-center" style="width: 23rem;">
                    <img src="${meal.strMealThumb}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h2>${meal.strMeal}</h2>
                        <h6>${meal.strCategory}</h6>
                      <!-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
                    </div>
                </div>
              </div>`;

                    arr += element;
                    let listOfMeals = document.getElementById('listOfMeals');
                    listOfMeals.innerHTML = arr;
                })
            });


    }
});